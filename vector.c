/* See LICENSE file for copyright and license details. */
#include <math.h>

#include "vector.h"

#define PI	3.14159265358979323846

/* Functions imlementations */
float*
vector_cart2pol(float x, float y)
{
	float pol[2];

	pol[0] = hypot(x, y);
	pol[1] = atan2(y, x);
	return pol;
}

float*
vector_cart2sph(float x, float y, float z)
{
	float sph[3];

	sph[0] = sqrt((x*x)+(y*y)+(z*z));
	sph[1] = PI/2-acos(z/sph[0]);
	sph[2] = atan(y/x);

	return sph;
}

float*
vector_cross(float* x, float* y)
{
	float cross[3];

	cross[0] = x[1]*y[2] - y[1]*x[2];
	cross[1] = -x[0]*y[2] + y[0]*x[2];
	cross[2] = x[0]*y[1] - y[0]*x[1];

	return cross;
}

float
vector_dot(float* x, float* y, int n)
{
	int i;
	float dot;

	dot = 0.0;

	for (i=0; i<n; i++)
		dot += x[i]*y[i];

	return dot;
}

float*
vector_pol2cart(float r, float phi)
{
	float cart[2];

	cart[0] = r*cos(phi);
	cart[1] = r*sin(phi);

	return cart;
}

