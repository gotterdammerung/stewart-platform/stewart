/* See LICENSE file for copyright and license details. */
#include <stdio.h>

#include "pid.h"

/* Functions imlementations */
float
pid_update(float kp, float ki, float kd, float ee, float *ed, float *ii,
		float ts)
{
	float dd, y;

	/* Check error */
	if (ee<-1.0)
		ee = -1.0;
	if (1.0<ee)
		ee = 1.0;

	/* Differential */
	dd = ee-(*ed);
	*ed = ee;

	/* Integral */
	*ii += ee*ts;
	if ((*ii)<-1.0)
		(*ii) = -1.0;
	if (1.0<(*ii))
		(*ii) = 1.0;

	/* Control */
	y = kp*ee + ki*(*ii) + kd*dd;

	printf("[pid.k] (%6.4f, %6.4f, %6.4f)\n"
		"[pid.y] (%6.4f, %6.4f, %6.4f)\n",
			ee, *ii, dd,
			kp*ee, ki*(*ii), kd*dd);

	return y;
}
