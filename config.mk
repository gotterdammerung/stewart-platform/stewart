# Stewart version
VERSION = 1.0

# Paths
PREFIX = /usr/local
MANPREFIX = ${PREFIX}/share/man

# Includes and libs
LIBS =

# Flags
CFLAGS  = -std=c99 -Os -D_POSIX_C_SOURCE=200809L -DVERSION=\"${VERSION}\"
LDFLAGS = ${LIBS}

# Compiler
CC = tcc
