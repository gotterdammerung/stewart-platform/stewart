/* See LICENSE file for copyright and license details. */
#include "control.h"

#include <math.h>
#include <stdio.h>
#include <sys/time.h>

#include "pid.h"
#include "stewartpos.h"

#include "config.h"

static struct timeval control_time;

int
control_pid(int x, int y, int *ang)
{
	int i, td;
	float nx, ny, ex, ey, yx, yy;

	struct timeval tt;

	printf("pos: %03d, %03d\n", x, y);

	nx = x/100.0f;
	ny = y/100.0f;

	ex = control_x.sp - nx;
	ey = -1*(control_y.sp - ny);

	/* Round error */
	ex = roundf(100*ex)/100.0;
	ey = roundf(100*ey)/100.0;

	if ((-tolerance<=ex)&&(ex<tolerance))
		ex = 0.0;
	if ((-tolerance<=ey)&&(ey<tolerance))
		ey = 0.0;

	printf("error: %3.2f, %3.2f\n", ex, ey);

	/* Timer */
	gettimeofday(&tt, NULL);
	td = (tt.tv_sec-control_time.tv_sec)*1000 +
		(tt.tv_usec-control_time.tv_usec)/1000;
	printf("time (ms): %d\n", td);
	gettimeofday(&control_time, NULL);

	/* PID */
	yx = pid_update(	control_x.kp,
				control_x.ki,
				control_x.kd,
				ex,
				&control_x.ed,
				&control_x.ii,
				td*1e-3
			);

	yy = pid_update(	control_y.kp,
				control_y.ki,
				control_y.kd,
				ey,
				&control_y.ed,
				&control_y.ii,
				td*1e-3
			);

	printf("pid: %4.3f, %4.3f\n", yx, yy);

	/* Calculate position */
	stewartpos(yx, yy, ang);
	printf("pos: ");
	for (i=0; i<6; i++)
		printf("%d ", ang[i] );
	printf("\n");


	return 0;
}

void
control_setup()
{
	/* Init axis x */
	control_x.kp = init_kp;
	control_x.ki = init_ki;
	control_x.kd = init_kd;

	control_x.ed = 0.0;

	control_x.sp = init_x;

	/* Init axis y */
	control_y.kp = init_kp;
	control_y.ki = init_ki;
	control_y.kd = init_kd;

	control_y.ed = 0.0;

	control_y.sp = init_y;

	/* Init stewart position */
	stewartpos_setup();

	/* Timer */
	gettimeofday(&control_time, NULL);
}
