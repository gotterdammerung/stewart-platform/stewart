#include "util.h"

#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>

void
die(const char *err, ...)
{
	va_list er;

	va_start(er, err);
	vfprintf(stderr, err, er);
	va_end(er);
	exit(1);
}
