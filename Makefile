# stewart

include config.mk

SRC = stewart.c control.c util.c pid.c stewartpos.c vector.c serial.c
OBJ = ${SRC:.c=.o}

all: options stewart
	./stewart -d /dev/ttyACM0 -b 115200

options:
	@echo stewart build options:
	@echo "CFLAGS   = ${CFLAGS}"
	@echo "LDFLAGS  = ${LDFLAGS}"
	@echo "CC       = ${CC}"

.c.o:
	${CC} -c ${CFLAGS} $<

${OBJ}: config.h config.mk

stewart: ${OBJ}
	${CC} -o $@ ${OBJ} ${LDFLAGS}

clean:
	rm -f stewart ${OBJ} stewart-${VERSION}.tar.xz

dist: clean
	mkdir -p stewart-${VERSION}
	cp -R ${SRC} config.h stewart.1\
		Makefile config.mk LICENSE README\
		client.h drw.h server.h util.h\
		stewart-${VERSION}
	tar -cf stewart-${VERSION}.tar stewart-${VERSION}
	xz -9 -T0 stewart-${VERSION}.tar
	rm -rf stewart-${VERSION}

install: all
	mkdir -p ${PREFIX}/bin
	cp -f stewart ${PREFIX}/bin
	chmod 755 ${PREFIX}/bin/stewart

uninstall:
	rm -f ${PREFIX}/bin/stewart\
		${MANPREFIX}/man1/stewart.1

.PHONY: all options clean dist install uninstall
