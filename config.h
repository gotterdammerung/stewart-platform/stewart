/* See LICENSE file for copyright and license details. */

/* Port */
static const int port = 60000;

/* PID */
static const float init_kp = 0.1;
static const float init_ki = 0.0;
static const float init_kd = 0.0;

/* Set point */
static const float init_x = 0.5;
static const float init_y = 0.5;
static const float tolerance = 0.15;

/* Platform */
static float stewartpos_bx = 48e-3;
static float stewartpos_by = 61e-3;

static float stewartpos_px = 46e-3;
static float stewartpos_py = 60e-3;

static float stewartpos_r = 150e-3;
static float stewartpos_m = 150e-3;
static float stewartpos_l = 31e-3;
