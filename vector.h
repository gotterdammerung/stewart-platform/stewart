/* See LICENSE file for copyright and license details. */
/* Functions declarations */
float* vector_cart2pol(float x, float y);
float* vector_cart2sph(float x, float y, float z);
float* vector_cross(float* x, float* y);
float vector_dot(float* x, float* y, int n);
float* vector_pol2cart(float r, float phi);
