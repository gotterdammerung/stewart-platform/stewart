/* See LICENSE file for copyright and license details. */
#include "stewartpos.h"

#include <stdio.h>
#include <math.h>

#include "vector.h"

#include "config.h"

#define PI	3.14159265358979323846

/* Variables */
static float stewart_b[6][3];
static float stewart_p[6][3];
static float n[3];

/* Functions implementations */
int
stewartpos(float x, float y, int *ang)
{
	int i, j;
	float px, py;
	float xx, yy;
	float rx[3], ry[3], tt[3];
	float beta[6];
	float newp[6][3];
	float *sph, *cross;

	/* Normal vector from platform */
	n[0] = x/stewartpos_r;
	n[1] = y/stewartpos_r;
	n[2] = sqrt((stewartpos_r*stewartpos_r)-(x*x)-(y*y)) / stewartpos_r;

	/* New base */
	rx[0] = 1;
	rx[1] = 0;

	rx[2] = -1*n[0]*rx[0]/n[2];
	cross = vector_cross(n, rx);

	for (i=0; i<3; i++)
		ry[i] = cross[i];

	/* Normalize base*/
	xx = sqrt(vector_dot(rx, rx, 3));
	yy = sqrt(vector_dot(ry, ry, 3));

	for (i=0; i<3; i++)
	{
		rx[i] /= xx;
		ry[i] /= yy;
	}

	/* Platform position */
	tt[0] = x;
	tt[1] = y;
	tt[2] = -(stewartpos_r-(n[2]*stewartpos_r));
	for (i=0; i<6; i++)
		for (j=0; j<3; j++)
			newp[i][j] =	stewart_p[i][0]*rx[j] +
					stewart_p[i][1]*ry[j] +
					stewart_p[i][2]*n[j] - tt[j];

	/* Arm distance */
	for (i=0; i<6; i++)
		for (j=0; j<3; j++)
			newp[i][j] = fabs(newp[i][j]-stewart_b[i][j]);

	/* Get servo angle */
	for (i=0; i<6; i++)
	{
		/* Get beta */
		beta[i] = sqrt(vector_dot(newp[i], newp[i], 3));
		beta[i] = acos( (	(stewartpos_l*stewartpos_l) +
					(beta[i]*beta[i]) -
					(stewartpos_m*stewartpos_m))
				/ (2*stewartpos_l*beta[i]));
		sph = vector_cart2sph(newp[i][0], newp[i][1], newp[i][2]);
		ang[i] = (sph[1]-beta[i])*180/PI + 90;
	}

	j = 0;
	for (i=0; i<6; i++)
		if ((ang[i]<0)||(180<ang[i]))
			j = 1;

	if (j==0)
		return 0;

	for (i=0; i<6; i++)
		ang[i] = 90;

	return 0;
}


int
stewartpos_setup()
{
	int i, j;

	float *pol, *cart;

	/* Base B0 */
	stewart_b[0][0] = stewartpos_bx;
	stewart_b[0][1] = stewartpos_by;
	stewart_b[0][2] = 0.0;

	/* Base B2 */
	pol = vector_cart2pol(stewartpos_bx, stewartpos_by);
	cart = vector_pol2cart(pol[0], pol[1]+(2*PI/3));
	stewart_b[2][0] = cart[0];
	stewart_b[2][1] = cart[1];
	stewart_b[2][2] = 0.0;

	/* Base B4 */
	pol = vector_cart2pol(stewartpos_bx, stewartpos_by);
	cart = vector_pol2cart(pol[0], pol[1]-(2*PI/3));
	stewart_b[4][0] = cart[0];
	stewart_b[4][1] = cart[1];
	stewart_b[4][2] = 0.0;

	/* Base B1 */
	stewart_b[1][0] = -stewartpos_bx;
	stewart_b[1][1] = stewartpos_by;
	stewart_b[1][2] = 0.0;

	/* Base B3 */
	pol = vector_cart2pol(-stewartpos_bx, stewartpos_by);
	cart = vector_pol2cart(pol[0], pol[1]+(2*PI/3));
	stewart_b[3][0] = cart[0];
	stewart_b[3][1] = cart[1];
	stewart_b[3][2] = 0.0;

	/* Base B5 */
	pol = vector_cart2pol(-stewartpos_bx, stewartpos_by);
	cart = vector_pol2cart(pol[0], pol[1]-(2*PI/3));
	stewart_b[5][0] = cart[0];
	stewart_b[5][1] = cart[1];
	stewart_b[5][2] = 0.0;

	/* Plataform P0 */
	stewart_p[0][0] = stewartpos_px;
	stewart_p[0][1] = stewartpos_py;
	stewart_p[0][2] = stewartpos_r;

	/* Plataform P2 */
	pol = vector_cart2pol(stewartpos_px, stewartpos_py);
	cart = vector_pol2cart(pol[0], pol[1]+(2*PI/3));
	stewart_p[2][0] = cart[0];
	stewart_p[2][1] = cart[1];
	stewart_p[2][2] = stewartpos_r;

	/* Plataform P4 */
	pol = vector_cart2pol(stewartpos_px, stewartpos_py);
	cart = vector_pol2cart(pol[0], pol[1]-(2*PI/3));
	stewart_p[4][0] = cart[0];
	stewart_p[4][1] = cart[1];
	stewart_p[4][2] = stewartpos_r;

	/* Plataform P1 */
	stewart_p[1][0] = -stewartpos_px;
	stewart_p[1][1] = stewartpos_py;
	stewart_p[1][2] = stewartpos_r;

	/* Plataform P3 */
	pol = vector_cart2pol(-stewartpos_px, stewartpos_py);
	cart = vector_pol2cart(pol[0], pol[1]+(2*PI/3));
	stewart_p[3][0] = cart[0];
	stewart_p[3][1] = cart[1];
	stewart_p[3][2] = stewartpos_r;

	/* Plataform P5 */
	pol = vector_cart2pol(-stewartpos_px, stewartpos_py);
	cart = vector_pol2cart(pol[0], pol[1]-(2*PI/3));
	stewart_p[5][0] = cart[0];
	stewart_p[5][1] = cart[1];
	stewart_p[5][2] = stewartpos_r;

	return 0;
}
