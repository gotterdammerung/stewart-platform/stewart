/* See LICENSE file for copyright and license details. */
/* Functions declarations */
float pid_update(float kp, float ki, float kd, float ee, float *ed,
		float *ii, float ts);
