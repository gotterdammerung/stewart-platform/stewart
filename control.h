/* See LICENSE file for copyright and license details. */

/* List */
typedef struct Axis Axis;

struct Axis
{
	float kp;
	float ki;
	float kd;
	float ee;
	float ed;
	float ii;
	float sp;
};

static Axis control_x, control_y;

/* Functions declarations */
int control_pid(int x, int y, int* ang);
void control_setup();
