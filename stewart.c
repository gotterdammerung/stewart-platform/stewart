/* See LICENSE file for copyright and license details. */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>

#include <sys/socket.h>
#include <netinet/in.h>

char*argv0;
#include "arg.h"
#include "util.h"
#include "control.h"

#include "config.h"

/* Functions declarations */
static void clientconnect(void);
static void getdata(void);
static void maincalculate(char *data);
static void usage(void);

/* Globals */
static struct sockaddr_in serversocket;
static int serverid;
static int clientid;
static char *serialdevice = NULL;
static int serialbaud = 0;

/* Functions implementations */
void
clientconnect()
{
	socklen_t socketsize;

	socketsize = sizeof(serversocket);
	while ((clientid = accept(serverid, (struct sockaddr *) &serversocket,
		&socketsize))<0);

	printf("client id: %d\n", clientid);
}

void
getdata()
{
	int n;
	char mess[10];

	memset(&mess[0], 0, sizeof(mess));

	/* Init PID control */
	control_setup();

	while (1)
	{
		if (read(clientid, mess, 10)==0)
			break;
		maincalculate(mess);
	}
}

void
maincalculate(char *data)
{
	char mess[4];
	char send[30];
	int x, y;
	int ang[6];

	/* Check */
	if (	(data[0]!='(')||
		(data[4]!=',')||
		(data[8]!=')'))
		return;

	/* Get coordinates */
	mess[0] = data[1];
	mess[1] = data[2];
	mess[2] = data[3];
	mess[3] = '\0';
	x = atoi(mess);

	mess[0] = data[5];
	mess[1] = data[6];
	mess[2] = data[7];
	mess[3] = '\0';
	y = atoi(mess);

	/* pid control */
	control_pid(x, y, ang);

	/* Send information */
	sprintf(send, "(%03d,%03d,%03d,%03d,%03d,%03d)",
			ang[0], ang[1], ang[2], ang[3], ang[4], ang[5]);
	while(serial_writechar(&send, sizeof(send)));
}

void
usage()
{
	die(	"usage: control [OPTION]\n"
		"  -v        Show version\n"
		"  -h        Help command\n" );
}

int
main(int argc, char *argv[])
{
	/* Arguments */
	ARGBEGIN {
	case 'b':
		serialbaud = atoi(EARGF(usage()));
		break;
	case 'd':
		serialdevice = EARGF(usage());
		break;
	case 'v':
		die("control-"VERSION"\n");
		return 0;
	default:
		usage();
	} ARGEND


	if (!serialbaud)
		serialbaud = 9600;

	if (!serialdevice)
	{
		die("No found device\n");
	}

	/* Open port */
	if (serial_openport(serialdevice, serialbaud)!=0)
	{
		die("%s no connect\n",serialdevice);
	}

	/* Socket init */
	serverid = socket(AF_INET, SOCK_STREAM, 0);
	if (serverid<0)
		die("Don't create socket.\n");

	memset(&serversocket, 0, sizeof(struct sockaddr_in));

	serversocket.sin_addr.s_addr = INADDR_ANY;
	serversocket.sin_family = AF_INET;
	serversocket.sin_port = htons(port);

	if (bind(serverid, (struct sockaddr *) &serversocket,
			sizeof(struct sockaddr_in)) != 0)
		die("Don't bind socket.\n");
	if (listen(serverid, 1) != 0)
		die("Error in listen\n");

	printf("Wait client.\n");

	clientconnect();
	getdata();

	/* Close socket*/
	close(serverid);

	return 0;
}
